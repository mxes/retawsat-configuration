@echo :: Restore new empty 7.6.0.1 into the Anywhere Sales Image
@echo :: Update SOURCE_DIR and RELEASE_DIR for other environments.
@echo :: 20150925
@echo :: jwmarsden@au1.ibm.com

set SOURCE_DIR=C:\App\Source\retawsat.git
set RELEASE_DIR=C:\maximo\maximo

@echo :: Deleting script files
del /q "%RELEASE_DIR%\tools\maximo\en\z_pbf\*.*"
del /q "%RELEASE_DIR%\tools\maximo\en\z_taseam\*.*"
del /q "%RELEASE_DIR%\tools\maximo\en\z_tasdev\*.*"

@echo :: Deleting product xml files
del /q "%RELEASE_DIR%\applications\maximo\properties\product\z_pbf.xml"
del /q "%RELEASE_DIR%\applications\maximo\properties\product\z_taseam.xml"
del /q "%RELEASE_DIR%\applications\maximo\properties\product\z_tasdev.xml"

rmdir /S/Q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_pbf"
rmdir /S/Q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_taseam"
rmdir /S/Q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_tasdev"

@echo ::Running maxinst.bat (VM uses MAXIMO for tablespace)
C:
CD %RELEASE_DIR%\tools\maximo
call anzmaxinst.bat -cen -imaximo.ora -sMAXIMO -tMAXIMO -umaximo -pmaximo

@echo ::Copy the source files for extracting master data / loading data and updating the product xml

copy "%SOURCE_DIR%\tools\maximo\anzmaxinst.bat" "%RELEASE_DIR%\tools\maximo" /Y
copy "%SOURCE_DIR%\tools\maximo\anzmaxinst.xml" "%RELEASE_DIR%\tools\maximo" /Y

@rem copy "%SOURCE_DIR%\tools\maximo\anzextractdata.bat" "%RELEASE_DIR%\tools\maximo" /Y
@rem copy "%SOURCE_DIR%\tools\maximo\anzextractdata.xml" "%RELEASE_DIR%\tools\maximo" /Y

copy "%SOURCE_DIR%\tools\maximo\anzloaddata.bat" "%RELEASE_DIR%\tools\maximo" /Y
copy "%SOURCE_DIR%\tools\maximo\anzloaddata.xml" "%RELEASE_DIR%\tools\maximo" /Y

copy "%SOURCE_DIR%\tools\maximo\anzupdateproductxml.bat" "%RELEASE_DIR%\tools\maximo" /Y
copy "%SOURCE_DIR%\tools\maximo\anzupdateproductxml.xml" "%RELEASE_DIR%\tools\maximo" /Y

@echo ::Copy the tool class files
del /q "%RELEASE_DIR%\tools\maximo\classes\com\ibm\anz\maximo\tools\*.*"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\classes\com\ibm\anz\maximo\tools\*.*" "%RELEASE_DIR%\tools\maximo\classes\com\ibm\anz\maximo\tools\"

mkdir "%RELEASE_DIR%\tools\maximo\classes\psdi\z_pbf\en"
mkdir "%RELEASE_DIR%\tools\maximo\classes\psdi\z_taseam\en"
mkdir "%RELEASE_DIR%\tools\maximo\classes\psdi\z_tasdev\en"

@echo :: Copy the script class files
del /q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_pbf\en\*.*"
del /q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_taseam\en\*.*"
del /q "%RELEASE_DIR%\tools\maximo\classes\psdi\z_tasdev\en\*.*"

xcopy /d/y "%SOURCE_DIR%\tools\maximo\classes\psdi\z_pbf\en" "%RELEASE_DIR%\tools\maximo\classes\psdi\z_pbf\en"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\classes\psdi\z_taseam\en" "%RELEASE_DIR%\tools\maximo\classes\psdi\z_taseam\en"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\classes\psdi\z_taseam\en" "%RELEASE_DIR%\tools\maximo\classes\psdi\z_tasdev\en"

@echo ::::::::::::::::::::::::::::::::::::::::::ANZ Database Configuration Installation:::::::::::::::::::::
@echo ::Copying z_pbf scripts from the shared folder to maximo folder
mkdir "%RELEASE_DIR%\tools\maximo\en\z_pbf"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\en\z_pbf" "%RELEASE_DIR%\tools\maximo\en\z_pbf"
@echo ::Copying ANZCOMMON product XML from the shared folder to WAS folder
copy "%SOURCE_DIR%\applications\maximo\properties\product\z_pbf.xml" "%RELEASE_DIR%\applications\maximo\properties\product\z_pbf.xml" /Y
@echo ::Update Product XML File for pz_pbf
C:
CD "%RELEASE_DIR%\tools\maximo"
call anzupdateproductxml.bat -pz_pbf

@echo ::Running updateDB.bat
CD "%RELEASE_DIR%\tools\maximo"
call updatedb.bat

@echo :::::::::::::::::::::::::::::::::::::::::: Tas Water Configuration Installation:::::::::::::::::::::
@echo ::Copying z_taseam scripts from the shared folder to maximo folder
mkdir "%RELEASE_DIR%\tools\maximo\en\z_taseam"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\en\z_taseam" "%RELEASE_DIR%\tools\maximo\en\z_taseam"
@echo ::Copying z_taseam product XML from the shared folder to WAS folder
copy "%SOURCE_DIR%\applications\maximo\properties\product\z_taseam.xml" "%RELEASE_DIR%\applications\maximo\properties\product\z_taseam.xml" /Y
@echo ::Update Product XML File for z_taseam
C:
CD "%RELEASE_DIR%\tools\maximo"
call anzupdateproductxml.bat -pz_taseam

@echo ::Running updateDB.bat
CD "%RELEASE_DIR%\tools\maximo"
call updatedb.bat

@echo Copy the EAM init data scripts to the maximo folder
del /q "%RELEASE_DIR%\tools\maximo\initdata\z_taseam\*.*
echo d | xcopy /d/y "%SOURCE_DIR%\tools\maximo\initdata\z_taseam" %RELEASE_DIR%\tools\maximo\initdata\z_taseam
@echo Load Initial data for z_taseam
C:
CD "%RELEASE_DIR%\tools\maximo"
call anzloaddata.bat -i
@echo Copy the EAM master data scripts to the maximo folder
del /q "%RELEASE_DIR%\tools\maximo\masterdata\z_taseam\*.*
echo d | xcopy /d/y "%SOURCE_DIR%\tools\maximo\masterdata\z_taseam" %RELEASE_DIR%\tools\maximo\masterdata\z_taseam
@echo Load Master data for z_taseam
C:
CD "%RELEASE_DIR%\tools\maximo"
call anzloaddata.bat -m

@echo :::::::::::::::::::::::::::::::::::::::::: Tas Water Development Configuration Installation:::::::::::::::::::::
@echo ::Copying z_tasdev scripts from the shared folder to maximo folder
mkdir "%RELEASE_DIR%\tools\maximo\en\z_tasdev"
xcopy /d/y "%SOURCE_DIR%\tools\maximo\en\z_tasdev" "%RELEASE_DIR%\tools\maximo\en\z_tasdev"
@echo ::Copying z_tasdev product XML from the shared folder to WAS folder
copy "%SOURCE_DIR%\applications\maximo\properties\product\z_tasdev.xml" "%RELEASE_DIR%\applications\maximo\properties\product\z_tasdev.xml" /Y
@echo ::Update Product XML File for z_tasdev
C:
CD "%RELEASE_DIR%\tools\maximo"
call anzupdateproductxml.bat -pz_tasdev

@echo ::Running updateDB.bat
CD "%RELEASE_DIR%\tools\maximo"
call updatedb.bat