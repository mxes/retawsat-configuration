/*
 * (C) COPYRIGHT IBM CORP. 2011,2012
 * The source code for this program is not published or otherwise
 * divested of its trade secrets, irrespective of what has been
 * deposited with the U.S. Copyright Office.
 * 
 */

package psdi.z_taseam.en;

import java.io.PrintStream;
import java.sql.Connection;
import java.util.HashMap;

import psdi.script.AutoUpgradeTemplate;

/**
 * Run Config DB during Update
 * 
 * @author jwmarsden@au1.ibm.com
 * 
 */
public class V7601_01 extends AutoUpgradeTemplate
{
//    private HashMap<String, Integer> maxIDs = new HashMap<String, Integer>();

//    public V7601_01(Connection con) throws Exception
//    {
//        super(con);
//    }
//
//    public V7601_01(Connection con, PrintStream ps) throws Exception
//    {
//        super(con, ps);
//    }

    public V7601_01(Connection con, HashMap<?,?> params, PrintStream ps) throws Exception
    {
        super(con, params, ps);
    }

    protected void init() throws Exception
    {
        super.init();
    }

    protected void process() throws Exception
    {
        super.process();
        runConfigRestoreAndDrop();
    }

}