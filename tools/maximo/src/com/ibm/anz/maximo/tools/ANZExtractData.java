package com.ibm.anz.maximo.tools;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import psdi.util.DBConnect;

public class ANZExtractData {
	protected Properties maxprops;
	protected ArrayList<String> excludeList;
	protected String outputFile;
	protected ArrayList<String> exceptionList;
	protected String where;

	protected String query1;
	protected static String query1a = "select servicename, objectname from maxobject where persistent = 1 and isview = 0";
	protected static String query1b = " and internal = 0 and objectname not like 'MFMAIL%' and objectname not like 'PLUSG_AS%' and objectname not like 'WF%' and objectname not like 'INB%' and objectname not like 'SKD%' and objectname not like 'REPORT%' ";
	protected static String query1c = " order by servicename, mainobject desc, objectname";
	protected static String query2 = "select a.attributeno, a.columnname, a.maxtype, "
			+ "		case when a.maxtype in ('UPPER', 'LONGALN', 'CLOB', 'GL', 'CRYPTO', 'ALN', 'LOWER', 'CRYPTOX') then 1 else 0 end as quote, "
			+ "		case when a.maxtype in ('DATETIME', 'DATE', 'TIME') then 1 else 0 end as dt, "
			+ "		s.sequencename "
			+ "from maxattribute a, maxsequence s where a.persistent = 1 and a.entityname = s.tbname(+) and a.columnname = s.name(+) and a.objectname = ? order by s.sequencename, a.attributeno";

	protected static String query4 = "select * from autokey order by autokeyname";

	protected static String[][] exceptionAttributes = { { "CLASSSPECUSEWITH", "CLASSSPECID" }, { "JOBTASK", "JOBPLANID" }, { "MEASUREUNIT", "CONTENTUID" },
			{ "CLASSSPEC", "ASSETATTRIBUTEID" },
			/**
			 * Added ContractID and JobPlan Attributes
			 */
			{ "CONTRACTAUTH", "CONTRACTID" },

			{ "JPTASKRELATION", "JOBPLANID" }, { "JOBPLANCLASS", "JOBPLANID" }, { "JOBMATERIAL", "JOBPLANID" }, { "JOBTOOL", "JOBPLANID" },
			{ "JOBLABOR", "JOBPLANID" }, { "JOBSERVICE", "JOBPLANID" }, { "JOBTASK", "JOBPLANID" }, { "JOBPLANSPEC", "REFOBJECTID" },
			{ "JPASSETSPLINK", "JOBPLANID" }, { "JOBITEM", "JOBPLANID" }, { "JPTASKLOOKUP", "JOBPLANID" } };

	protected static String[][] ignoreSequenceAttributes = { { "FAILURECODE", "FAILURECODEID" }, { "CLASSSPEC", "CLASSSPECID" } };

	protected class Attr {
		public int attributeNo;
		public String columnName;
		public String maxtype;
		public boolean quote;
		public boolean dt;
		public String sequenceName;
	};

	protected SimpleDateFormat sdf;

	public ANZExtractData() {
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		exceptionList = new ArrayList<String>();
		for (int i = 0; i < exceptionAttributes.length; i++)
			exceptionList.add(exceptionAttributes[i][0] + "." + exceptionAttributes[i][1]);
	}

	public void setup(HashMap<String, String> paramHashMap) {
		maxprops = new Properties();
		try {
			maxprops.load(getClass().getResource("/maximo.properties").openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		excludeList = new ArrayList<String>();
		if (paramHashMap.get("-e") != null) {
			String[] excludeParts = paramHashMap.get("-e").split(",");
			for (int i = 0; i < excludeParts.length; i++)
				excludeList.add(excludeParts[i]);
		}

		outputFile = "en/apamasterdata.sql";
		if (paramHashMap.get("-o") != null) {
			outputFile = paramHashMap.get("-o");
		}

		where = null;
		if (paramHashMap.get("-w") != null) {
			where = paramHashMap.get("-w");
		}

		if (paramHashMap.get("-i") != null) {
			String includeList = paramHashMap.get("-i");
			includeList = "'" + includeList.replace(",", "','") + "'";
			query1 = query1a + " and objectname in (" + includeList + ")" + query1c;
			excludeList.clear();
		} else {
			query1 = query1a + query1b + query1c;
		}
	}

	public void extract() {
		Connection conn = null;

		try {
			Driver localDriver = (Driver) Class.forName(maxprops.getProperty("mxe.db.driver", "sun.jdbc.odbc.JdbcOdbcDriver")).newInstance();
			DriverManager.registerDriver(localDriver);
			String dbUrl = maxprops.getProperty("mxe.db.url");
			String dbUser = maxprops.getProperty("mxe.db.user");
			String dbPassword = maxprops.getProperty("mxe.db.password");
			String dbSchema = maxprops.getProperty("mxe.db.schemaowner");
			conn = DBConnect.getConnection(dbUrl, dbUser, dbPassword, dbSchema);

			PrintWriter pw = new PrintWriter(outputFile);
			String serviceName = "";
			boolean serviceEmpty = true;

			Statement stmt1 = conn.createStatement();
			ResultSet rs1 = stmt1.executeQuery(query1);

			System.out.println(query1);

			while (rs1.next()) {
				ArrayList<Attr> attrList = new ArrayList<Attr>();
				String objectName = rs1.getString("objectname");
				if (!rs1.getString("servicename").equals(serviceName)) {
					serviceName = rs1.getString("servicename");
					serviceEmpty = true;
				}

				if (!excludeList.contains(objectName)) {
					PreparedStatement stmt2 = conn.prepareStatement(query2);
					stmt2.setString(1, objectName);
					ResultSet rs2 = stmt2.executeQuery();
					while (rs2.next()) {
						Attr attr = new Attr();
						attr.attributeNo = rs2.getInt("attributeno");
						attr.columnName = rs2.getString("columnname");
						attr.maxtype = rs2.getString("maxtype");
						attr.quote = rs2.getBoolean("quote");
						attr.dt = rs2.getBoolean("dt");
						attr.sequenceName = rs2.getString("sequencename");
						attrList.add(attr);
					}
					rs2.close();
					stmt2.close();

					String dataQuery = "select * from " + objectName;
					if (where != null)
						dataQuery = dataQuery + " where " + where;
					Statement stmt3 = conn.createStatement();
					ResultSet rs3 = stmt3.executeQuery(dataQuery);
					boolean tableEmpty = true;
					while (rs3.next()) {
						if (serviceEmpty) {
							pw.println("-- " + serviceName + " tables\r\n");
							serviceEmpty = false;
						}
						if (tableEmpty) {
							System.out.println(objectName);
							String deleteString = "delete from " + objectName;
							if (where != null)
								deleteString = deleteString + " where " + where;
							pw.println(deleteString + ";");
							tableEmpty = false;
						}
						String str1 = "insert into " + objectName + " (";
						String str2 = "values (";
						for (int i = 0; i < attrList.size(); i++) {
							Attr attr = attrList.get(i);
							String val = rs3.getString(attr.columnName);
							if (!rs3.wasNull()) {
								str1 = str1 + attr.columnName + ", ";
								if (exceptionList.contains(objectName + "." + attr.columnName))
									val = getExceptionColumnValue(objectName, attr.columnName, rs3);
								else if (attr.sequenceName != null && !ignoreSequence(objectName, attr.columnName, rs3))
									val = attr.sequenceName + ".NEXTVAL";
								else if (attr.dt)
									val = "TO_DATE('" + sdf.format(rs3.getTimestamp(attr.columnName)) + "', 'YYYY-MM-DD HH24:MI')";
								else if (attr.quote)
									val = "'" + val.replace("'", "''") + "'";
								str2 = str2 + val + ", ";
							}
						}
						String str = str1.substring(0, str1.length() - 2) + ") " + str2.substring(0, str2.length() - 2) + "); ";
						pw.println(str);
					}
					rs3.close();
					stmt3.close();
					if (!tableEmpty) {
						pw.println("");
					}
				}
			}
			rs1.close();
			stmt1.close();

			Statement stmt4 = conn.createStatement();
			ResultSet rs4 = stmt4.executeQuery(query4);
			pw.println("--AUTOKEY update\r\n");
			while (rs4.next()) {
				String str = "update autokey set seed = " + rs4.getString("seed") + " where autokeyname = '" + rs4.getString("autokeyname") + "'";
				String org = rs4.getString(("orgid"));
				if (!rs4.wasNull())
					str += " and orgid = '" + org + "'";
				else
					str += " and orgid is null";
				String site = rs4.getString(("siteid"));
				if (!rs4.wasNull())
					str += " and siteid = '" + site + "'";
				else
					str += " and siteid is null";
				String set = rs4.getString(("setid"));
				if (!rs4.wasNull())
					str += " and setid = '" + set + "'";
				else
					str += " and setid is null";
				str += ";";
				pw.println(str);
			}
			rs4.close();
			stmt4.close();

			conn.close();
			pw.close();
		} catch (Exception localException) {
			localException.printStackTrace();
		}

	}

	public boolean ignoreSequence(String objectName, String columnName, ResultSet rs3) {
		if (objectName.equals("FAILURECODE") && columnName.equals("FAILURECODEID")) {
			return true;
		}
		if (objectName.equals("CLASSSPEC") && columnName.equals("CLASSSPECID")) {
			return true;
		}
		return false;
	}

	public String getExceptionColumnValue(String objectName, String columnName, ResultSet rs) throws SQLException {
		String val = null;
		if (objectName.equals("CLASSSPECUSEWITH") && columnName.equals("CLASSSPECID")) {
			val = "(select classspecid from classspec where classstructureid = '" + rs.getString("classstructureid") + "' and assetattrid = '"
					+ rs.getString("assetattrid") + "')";
		}

		if (objectName.equals("JOBTASK") && columnName.equals("JOBPLANID")) {
			val = "(select jobplanid from jobplan where jpnum = '" + rs.getString("jpnum") + "')";
		}

		if (objectName.equals("MEASUREUNIT") && columnName.equals("CONTENTUID")) {
			val = "MEASUREUNITSEQ.NEXTVAL";
		}

		if (objectName.equals("CLASSSPEC") && columnName.equals("ASSETATTRIBUTEID")) {
			val = "(select ASSETATTRIBUTEID from ASSETATTRIBUTE where ASSETATTRID = '" + rs.getString("assetattrid") + "')";
		}

		/**
		 * JWM - Add ContractID lookup for CONTRACTAUTH
		 */
		if (objectName.equals("CONTRACTAUTH") && columnName.equals("CONTRACTID")) {
			val = "(select CONTRACTID from CONTRACT where CONTRACTNUM = '" + rs.getString("contractnum") + "' AND REVISIONNUM = '"
					+ rs.getString("revisionnum") + "')";
		}

		/**
		 * JWM - Add all JobPlanID lookups for related tables
		 */
		if (objectName.equals("JPTASKRELATION") && columnName.equals("JOBPLANID") || objectName.equals("JOBMATERIAL") && columnName.equals("JOBPLANID")
				|| objectName.equals("JOBTOOL") && columnName.equals("JOBPLANID") || objectName.equals("JOBLABOR") && columnName.equals("JOBPLANID")
				|| objectName.equals("JOBSERVICE") && columnName.equals("JOBPLANID") || objectName.equals("JOBSERVICE") && columnName.equals("JOBPLANID")
				|| objectName.equals("JOBTASK") && columnName.equals("JOBPLANID") || objectName.equals("JOBPLANSPEC") && columnName.equals("REFOBJECTID")
				|| objectName.equals("JPASSETSPLINK") && columnName.equals("JOBPLANID") || objectName.equals("JOBITEM") && columnName.equals("JOBPLANID")
				|| objectName.equals("JPTASKLOOKUP") && columnName.equals("JOBPLANID")) {
			try {
				val = "(select jobplanid from jobplan where jpnum = '" + rs.getString("jpnum") + "')";
			} catch (Exception e) {
				System.out.println("Error extracting jpnum from " + objectName + "." + columnName);
			}
		}

		/**
		 * JWM - JOBPLANCLASS has no JPNUM, additional lookup required to
		 * connect jobplanid.
		 * 
		 * If this is continually used, this should be made nicer.
		 */
		if (objectName.equals("JOBPLANCLASS") && columnName.equals("JOBPLANID")) {
			Connection conn = null;
			String dbUrl = maxprops.getProperty("mxe.db.url");
			String dbUser = maxprops.getProperty("mxe.db.user");
			String dbPassword = maxprops.getProperty("mxe.db.password");
			String dbSchema = maxprops.getProperty("mxe.db.schemaowner");
			try {
				conn = DBConnect.getConnection(dbUrl, dbUser, dbPassword, dbSchema);

				String query = "SELECT JPNUM FROM JOBPLAN WHERE JOBPLANID = '" + rs.getString(columnName) + "'";

				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query);

				if (resultSet.next()) {
					val = "(select jobplanid from jobplan where jpnum = '" + resultSet.getString("jpnum") + "')";
				} else {
					val = rs.getString(columnName);
				}

				resultSet.close();
				statement.close();

				conn.close();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					conn.close();
				}
			}

		}

		return val;
	}

	public static void main(String[] args) {
		ANZExtractData apaExtractData = new ANZExtractData();
		HashMap<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String str1 = args[i].substring(0, 2);
			String str2 = args[i].substring(2);
			if ((str2 == null) || (str2.length() <= 0))
				str2 = "";
			if ((str1.equalsIgnoreCase("-h")) || (str1.equalsIgnoreCase("-help"))) {
				System.out
						.println("Usage : -f<properties filename> -d<output directory> -o<output filename> -a<db alias> -u<username> -p<password>  -h<help>  -n<input directory> -n<class directory> -z<db2text output> -v<customer xml check> -r<install properties run>\n");
				System.exit(0);
			} else {
				params.put(str1.toLowerCase(), str2);
			}
		}
		apaExtractData.setup(params);
		apaExtractData.extract();
	}
}
