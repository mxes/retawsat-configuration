package com.ibm.anz.maximo.tools;

import java.io.File;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Update the product XML file to the current release.
 */
public class ANZUpdateProductXML {
	protected Properties maxprops;
	protected String product;

	public void setup(HashMap<String, String> paramHashMap) {
		maxprops = new Properties();
		if (paramHashMap.get("-p") != null) {
			product = paramHashMap.get("-p");
		}
	}

	public void update() {
		String buildNumber = null;
		String dbVersion = null;
		String releaseNumber = null;
		String dbScriptsFolder = null;
		int lastScriptSequence = 0;
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmm");
			buildNumber = df.format(new Date());
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(getClass().getResourceAsStream("/product/" + product + ".xml"));

			Element rootNode = doc.getDocumentElement();
			Element versionNode = (Element) rootNode.getElementsByTagName("version").item(0);
			String major = versionNode.getElementsByTagName("major").item(0).getTextContent();
			String minor = versionNode.getElementsByTagName("minor").item(0).getTextContent();
			String mod = versionNode.getElementsByTagName("modlevel").item(0).getTextContent();
			String patch = versionNode.getElementsByTagName("patch").item(0).getTextContent();
			releaseNumber = "V" + major + minor + mod + patch;
			dbScriptsFolder = rootNode.getElementsByTagName("dbscripts").item(0).getTextContent();
			Element buildNode = (Element) versionNode.getElementsByTagName("build").item(0);
			Element dbVersionNode = (Element) rootNode.getElementsByTagName("dbversion").item(0);
			Element lastDbVersionNode = (Element) rootNode.getElementsByTagName("lastdbversion").item(0);

			String lastDbVersion = releaseNumber + "-" + String.format("%02d", new Object[] { Integer.valueOf(lastScriptSequence) });

			File dbScriptsFolderFile = new File("en\\" + dbScriptsFolder);
			String[] list = dbScriptsFolderFile.list();
			if (list != null && Array.getLength(list) > 0) {
				for (int i = 0; i < list.length; i++) {
					String script = list[i].substring(0, list[i].indexOf("."));
					int number = -1;
					try {
						number = Integer.parseInt(script.substring(script.indexOf("_") + 1));
					} catch (Exception e) {
						number = -1;
					}
					if (number > lastScriptSequence)
						lastScriptSequence = number;
				}
			}

			dbVersion = releaseNumber + "-" + String.format("%02d", new Object[] { Integer.valueOf(lastScriptSequence) });
			buildNode.setTextContent(buildNumber);
			dbVersionNode.setTextContent(dbVersion);
			lastDbVersionNode.setTextContent(lastDbVersion);
			Source source = new DOMSource(doc);
			File file = new File("..\\..\\applications\\maximo\\properties\\product\\" + product + ".xml");
			Result result = new StreamResult(file);
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.transform(source, result);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ANZUpdateProductXML apaUpdateProductXML = new ANZUpdateProductXML();
		HashMap<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String str1 = args[i].substring(0, 2);
			String str2 = args[i].substring(2);
			if ((str2 == null) || (str2.length() <= 0))
				str2 = "";
			if ((str1.equalsIgnoreCase("-h")) || (str1.equalsIgnoreCase("-help"))) {
				System.out.println("Usage : -p<product>\n");
				System.exit(0);
			} else {
				params.put(str1.toLowerCase(), str2);
			}
		}
		apaUpdateProductXML.setup(params);
		apaUpdateProductXML.update();
	}
}
