package com.ibm.anz.maximo.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import psdi.util.DBConnect;

public class ANZLoadData {
	protected Properties maxprops;
	protected String folder;
	protected ArrayList<Product> products;

	public class Product {
		public String name;
		public String folder;
		public String maxvar;
		public String depends;
		public boolean processed;

		public Product() {
			processed = false;
		}
	};

	public ANZLoadData() {
		products = new ArrayList<Product>();
	}

	public void setup(HashMap<String, String> paramHashMap) {
		maxprops = new Properties();
		try {
			maxprops.load(getClass().getResource("/maximo.properties").openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		folder = "";
		if (paramHashMap.get("-i") != null) {
			folder = "initdata";
		}
		if (paramHashMap.get("-m") != null) {
			folder = "masterdata";
		}

	}

	public void loadProducts() {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			URL dir = getClass().getResource("/product");
			String[] list = (new File(dir.toURI())).list();

			for (int i = 0; i < list.length; i++) {
				String name = list[i];
				if(name.contains(".xml")) {
					Product product = new Product();
					product.name = name.substring(0, name.indexOf(".xml"));
					Document doc = docBuilder.parse(getClass().getResourceAsStream("/product/" + list[i]));
					Element rootNode = doc.getDocumentElement();
					product.folder = rootNode.getElementsByTagName("dbscripts").item(0).getTextContent();
					product.maxvar = rootNode.getElementsByTagName("dbmaxvarname").item(0).getTextContent();
					if (rootNode.getElementsByTagName("depends").getLength() > 0)
						product.depends = rootNode.getElementsByTagName("depends").item(0).getTextContent();
					products.add(product);
				} else {
					System.out.println("Skipping: " + name);
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void load() {
		Connection conn = null;

		try {
			Driver localDriver = (Driver) Class.forName(maxprops.getProperty("mxe.db.driver", "sun.jdbc.odbc.JdbcOdbcDriver")).newInstance();
			DriverManager.registerDriver(localDriver);
			String dbUrl = maxprops.getProperty("mxe.db.url");
			String dbUser = maxprops.getProperty("mxe.db.user");
			String dbPassword = maxprops.getProperty("mxe.db.password");
			String dbSchema = maxprops.getProperty("mxe.db.schemaowner");
			conn = DBConnect.getConnection(dbUrl, dbUser, dbPassword, dbSchema);

			for (int i = 0; i < products.size(); i++) {
				Product product = products.get(i);
				boolean dependencyMet = true;
				if (product.depends != null) {
					for (int j = 0; j < products.size(); j++) {
						if (products.get(j).name.equals(product.depends)) {
							if (products.get(j).processed = false) {
								dependencyMet = false;
							}
						}
					}
				}
				if (!dependencyMet) {
					products.remove(i);
					products.add(product);
					i--;
				} else {
					String fileName = folder + "/" + product.folder;
					File dir = new File(fileName);
					if (dir.exists()) {
						File[] list = dir.listFiles();
						if (list.length > 0) {
							String varname = product.maxvar + "_" + folder.toUpperCase();
							if (varname.length() > 18)
								varname = varname.substring(0, 18);
							String lastScript = null;
							int lastNumber = 0;
							Statement stmt = conn.createStatement();
							ResultSet rs = stmt.executeQuery("select varvalue from maxvars where varname = '" + varname + "'");
							if (rs.next())
								lastScript = rs.getString("varvalue");
							rs.close();
							stmt.close();

							if (lastScript != null) {
								lastNumber = Integer.parseInt(lastScript.substring(lastScript.indexOf("_") + 1));
							}

							String mostRecentScript = lastScript;

							for (int j = 0; j < list.length; j++) {
								File scriptFile = list[j];
								if (!scriptFile.getName().contains(".")) {
									String message = "No extension on file: " + folder + "/" + product.folder + "/" + scriptFile.getName();
									System.out.println(message);
									throw new RuntimeException(message);
								}
								String script = scriptFile.getName().substring(0, scriptFile.getName().indexOf("."));
								int number = Integer.parseInt(script.substring(script.indexOf("_") + 1));
								if (number > lastNumber) {
									System.out.println("Executing script : " + folder + "/" + product.folder + "/" + script);
									runSQLScript(scriptFile, conn);
									mostRecentScript = script;
								}
							}

							stmt = conn.createStatement();
							if (lastScript == null) {
								stmt.executeUpdate("insert into maxvars (maxvarsid, varname, varvalue) values (maxvarsseq.nextval, '" + varname + "', '"
										+ mostRecentScript + "')");
							} else {
								stmt.executeUpdate("update maxvars set varvalue = '" + mostRecentScript + "' where varname = '" + varname + "'");
							}
							stmt.close();

						}
					}

					product.processed = true;
				}
			}

			conn.close();
		} catch (Exception localException) {
			localException.printStackTrace();
		}

	}

	public void runSQLScript(File file, Connection conn) throws IOException, SQLException {
		boolean inQuote = false;
		boolean inComment = false;
		boolean lastWasDash = false;
		StringBuilder sb = new StringBuilder();
		FileInputStream fis = new FileInputStream(file);
		int cr = -1;
		while ((cr = fis.read()) != -1) {
			char c = (char) cr;

			if (c == '\r' || c == '\n')
				inComment = false;

			if (!inComment && c == '\'')
				inQuote = !inQuote;

			if (c == '-' && !inQuote) {
				if (!inComment && lastWasDash) {
					inComment = true;
					if(sb.length() == 1) {
						sb = new StringBuilder();
					} else if(sb.length() - 2 > 0) {
						sb.delete(sb.length() - 2, sb.length());
					}
				}
				lastWasDash = true;
			} else {
				lastWasDash = false;
			}

			if (!inComment)
				sb.append(c);

			if (c == ';' && !inQuote && !inComment) {
				Statement stmt = conn.createStatement();
				String sql = sb.toString().substring(0, sb.length() - 1).trim();
				
				//System.out.println("\"" + sql + "\"");
				stmt.executeUpdate(sql);
				System.out.println(sql);
				stmt.close();
				sb = new StringBuilder();
			}

		}
		fis.close();
	}

	public static void main(String[] args) {
		ANZLoadData apaLoadData = new ANZLoadData();
		HashMap<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String str1 = args[i].substring(0, 2);
			String str2 = args[i].substring(2);
			if ((str2 == null) || (str2.length() <= 0))
				str2 = "";
			if ((str1.equalsIgnoreCase("-h")) || (str1.equalsIgnoreCase("-help"))) {
				System.out.println("Usage : [-i | -m]\n");
				System.exit(0);
			} else {
				params.put(str1.toLowerCase(), str2);
			}
		}
		apaLoadData.setup(params);
		apaLoadData.loadProducts();
		apaLoadData.load();
	}

}
