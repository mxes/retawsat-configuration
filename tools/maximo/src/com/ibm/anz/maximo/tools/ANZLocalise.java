package com.ibm.anz.maximo.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import psdi.util.DBConnect;

public class ANZLocalise {
	protected Properties maxprops;
	protected Properties localisations;

	protected static String updateEndPoint = "update maxendpointdtl set value = ? where endpointname = ? and property = ?";
	protected static String updateEndPointPwd = "update maxendpointdtl set password = ? where endpointname = ? and property = ?";
	protected static String updateProp = "update maxpropvalue set propvalue = ? where propname = ?";
	protected static String updateInboundCommCfg = "update inboundcommcfg set [prop]=? where wfprocess = ?";
	protected static String updateInbCommSecurity = "update inbcommsecurity set [prop]=? where app = ?";
	protected static String updateDoNotRunProp = "update MAXPROPINSTANCE set propvalue = ? where propname = ? and servername = ?";
	protected static String deleteInstanceProp = "delete FROM MAXPROPINSTANCE where propname = ? and servername = ?";
	protected static String insertDoNotRunInstanceProp = "insert into MAXPROPINSTANCE (CHANGEBY,CHANGEDATE,ENCRYPTEDVALUE,MAXPROPVALUEID,PROPNAME,PROPVALUE,SERVERHOST,SERVERNAME) VALUES ('MAXADMIN',SYSDATE,null,MAXPROPVALUESEQ.NEXTVAL,?,'',null,?)";
	protected static String selectCrontaskVal = "select value from CRONTASKPARAM where crontaskname = 'LDAPSYNC' and instancename = 'LDAPSYNC01' and parameter = 'UserMapping'";
	protected static String updateCntskUserParam = "update crontaskparam set value = ? where crontaskname = 'LDAPSYNC' and instancename = 'LDAPSYNC01' and parameter = 'UserMapping'";
	protected static String updateDoctypePath = "update doctypes set defaultfilepath = ? where doctype = ?";

	public ANZLocalise() {
	}

	public void setup(HashMap<String, String> paramHashMap) {
		maxprops = new Properties();
		localisations = new Properties();

		try {
			maxprops.load(getClass().getResource("/maximo.properties").openStream());
			localisations.load(new FileInputStream("localisations.properties"));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void run() {
		Connection conn = null;
		try {
			Driver localDriver = (Driver) Class.forName(maxprops.getProperty("mxe.db.driver", "sun.jdbc.odbc.JdbcOdbcDriver")).newInstance();
			DriverManager.registerDriver(localDriver);
			String dbUrl = maxprops.getProperty("mxe.db.url");
			String dbUser = maxprops.getProperty("mxe.db.user");
			String dbPassword = maxprops.getProperty("mxe.db.password");
			String dbSchema = maxprops.getProperty("mxe.db.schemaowner");
			conn = DBConnect.getConnection(dbUrl, dbUser, dbPassword, dbSchema);

			String[] keys = new String[localisations.keySet().size()];
			Iterator<Object> it = localisations.keySet().iterator();
			for (int i = 0; i < keys.length; i++)
				keys[i] = (String) it.next();

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("ep.")) {
					String[] pair = keys[i].substring(3).split("\\.");
					if (pair.length == 2) {
						String name = pair[0];
						String prop = pair[1];
						String val = localisations.getProperty(keys[i]);

						if (prop.equals("PASSWORD")) {
							PreparedStatement setPswStmt = conn.prepareStatement(updateEndPointPwd);
							setPswStmt.setString(1, val);
							setPswStmt.setString(2, name);
							setPswStmt.setString(3, prop);
							setPswStmt.execute();
						} else {
							PreparedStatement stmt = conn.prepareStatement(updateEndPoint);
							stmt.setString(1, val);
							stmt.setString(2, name);
							stmt.setString(3, prop);
							stmt.execute();
						}
						System.out.println("Updated end point " + name + "  " + prop + " = " + val);
					}
				}
			}

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("prop.")) {
					String prop = keys[i].substring(5);
					String val = localisations.getProperty(keys[i]);

					if (prop.startsWith("mxe.crontask.donotrun")) {
						String server = prop.substring(22);
						prop = "mxe.crontask.donotrun";

						PreparedStatement delStmt = conn.prepareStatement(deleteInstanceProp);
						delStmt.setString(1, prop);
						delStmt.setString(2, server);
						delStmt.execute();

						PreparedStatement setPropStmt = conn.prepareStatement(insertDoNotRunInstanceProp);
						setPropStmt.setString(1, prop);
						setPropStmt.setString(2, server);
						setPropStmt.execute();

						PreparedStatement setNoRunStmt = conn.prepareStatement(updateDoNotRunProp);
						setNoRunStmt.setString(1, val);
						setNoRunStmt.setString(2, prop);
						setNoRunStmt.setString(3, server);
						setNoRunStmt.execute();
						System.out.println("Updated crontask " + prop + "." + server + " = " + val);
					} else {
						PreparedStatement stmt = conn.prepareStatement(updateProp);
						stmt.setString(1, val);
						stmt.setString(2, prop);
						stmt.execute();
						System.out.println("Updated property " + prop + " = " + val);
					}
				}
			}

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("inbcomcfg.")) {
					String[] pair = keys[i].substring(10).split("\\.");
					if (pair.length == 2) {
						String wfprocess = pair[0];
						String prop = pair[1];
						String val = localisations.getProperty(keys[i]);

						PreparedStatement stmt = conn.prepareStatement(updateInboundCommCfg.replace("[prop]", prop));
						stmt.setString(1, val);
						stmt.setString(2, wfprocess);
						stmt.execute();
						System.out.println("Updated inbound communication configuration for " + wfprocess + " : " + prop + " = " + val);
					}
				}
			}

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("inbcomsec.")) {
					String[] pair = keys[i].substring(10).split("\\.");
					if (pair.length == 2) {
						String app = pair[0];
						String prop = pair[1];
						String val = localisations.getProperty(keys[i]);

						PreparedStatement stmt = conn.prepareStatement(updateInbCommSecurity.replace("[prop]", prop));
						stmt.setString(1, val);
						stmt.setString(2, app);
						stmt.execute();
						System.out.println("Updated inbound communication security for " + app + " : " + prop + " = " + val);
					}
				}
			}

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("crontsk.")) {

					String crontaskparam = keys[i].substring(8);
					String val = localisations.getProperty(keys[i]);
					String usersgroup = "{ldapusergroup}";
					String existingval = null;
					String newval = null;
					PreparedStatement selectstm = conn.prepareStatement(selectCrontaskVal);
					ResultSet resultstm = selectstm.executeQuery();

					while (resultstm.next()) {
						existingval = resultstm.getString("VALUE");
					}

					newval = existingval.replace(usersgroup, val);

					PreparedStatement updstm = conn.prepareStatement(updateCntskUserParam);
					updstm.setString(1, newval);
					updstm.execute();
					System.out.println("Updated crontask " + crontaskparam + ": " + newval);
				}
			}

			for (int i = 0; i < keys.length; i++) {
				if (keys[i].startsWith("doclinks.")) {
					String doctype = keys[i].substring(25);
					String dfpath = localisations.getProperty(keys[i]);

					PreparedStatement updstm = conn.prepareStatement(updateDoctypePath);
					updstm.setString(1, dfpath);
					updstm.setString(2, doctype);
					updstm.execute();
					System.out.println("Updated Doclink defaultfilepath: " + doctype + ": " + dfpath);
				}
			}
			conn.close();
		}

		catch (Exception localException) {
			localException.printStackTrace();
		}

	}

	public static void main(String[] args) {
		ANZLocalise localise = new ANZLocalise();
		HashMap<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String str1 = args[i].substring(0, 2);
			String str2 = args[i].substring(2);
			if ((str2 == null) || (str2.length() <= 0))
				str2 = "";
			if ((str1.equalsIgnoreCase("-h")) || (str1.equalsIgnoreCase("-help"))) {
				System.out.println("Usage : \n");
				System.exit(0);
			} else {
				params.put(str1.toLowerCase(), str2);
			}
		}
		localise.setup(params);
		localise.run();
	}
}
