@ECHO OFF

SETLOCAL

set SCRIPT_DIR=%~dp0
set SCRIPT_NAME=anzloaddata.xml
set MAXIMO_SCRIPT_CP=%SCRIPT_DIR%\internal

if "%MAXIMO_JAVA%"=="" set MAXIMO_JAVA=%SCRIPT_DIR%\..\java\jre\bin\java.exe

@"%MAXIMO_JAVA%" -cp "%MAXIMO_SCRIPT_CP%" MaximoScript "%SCRIPT_DIR%%SCRIPT_NAME%" %*

exit /B %errorlevel%
