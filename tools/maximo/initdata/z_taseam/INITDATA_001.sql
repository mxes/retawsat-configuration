------------------------------------------------------------------------------------------------
-- Ensure MAXADMIN is accessable and has authority. 
--
-- Date     Author                    Description
-- -------- ------------------------- ----------------------------------------------------------
-- 20150923 jwmarsden@au1.ibm.com     Initial File
------------------------------------------------------------------------------------------------

-- Unlock MAXUSER and Set Password (Maximo01)
UPDATE MAXUSER SET STATUS = 'ACTIVE', FORCEEXPIRATION = 0, PASSWORD = '2EDCD82839A15059B4248E2850A87453' WHERE USERID = 'MAXADMIN';

-- Give full access to MAXADMIN
INSERT INTO APPLICATIONAUTH (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID)
SELECT 'MAXADMIN', SO.APP, SO.OPTIONNAME, APPLICATIONAUTHSEQ.NEXTVAL
FROM SIGOPTION SO
LEFT JOIN APPLICATIONAUTH AA ON SO.APP = AA.APP AND SO.OPTIONNAME = AA.OPTIONNAME AND AA.GROUPNAME = 'MAXADMIN'
WHERE AA.GROUPNAME IS NULL;